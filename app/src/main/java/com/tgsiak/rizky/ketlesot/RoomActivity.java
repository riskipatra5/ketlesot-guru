package com.tgsiak.rizky.ketlesot;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RoomActivity extends AppCompatActivity {
 LinearLayout btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        btn = findViewById(R.id.contoh1);

        TextView logofont = findViewById(R.id.team1);
        Typeface custom_fonts = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        logofont.setTypeface(custom_fonts);
        TextView logofont1 = findViewById(R.id.team2);
        Typeface custom_fonts1 = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        logofont1.setTypeface(custom_fonts1);
        TextView logofont2 = findViewById(R.id.team3);
        Typeface custom_fonts2 = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        logofont2.setTypeface(custom_fonts2);
        TextView logofont3 = findViewById(R.id.team4);
        Typeface custom_fonts3 = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        logofont3.setTypeface(custom_fonts3);

        btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

          setContentView(R.layout.dialog_view);

                return false;
            }
        });
    }
    public void dashboard(View v){
        Intent intent = new Intent(this,DashboardActivity.class);
        startActivity(intent);
    }
}
